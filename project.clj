(defproject cldj "master-SNAPSHOT"
  :description ""
  :license {:name "ISC Licence"}
  :dependencies [[org.clojure/clojure "1.10.3"]

                 ;; discljord
                 [org.suskalo/discljord "1.2.3"]

                 ;; web server
                 ;; [ring "1.9.4"]
                 ;; [ring/ring-json "0.5.1"]
                 [aleph "0.4.7-alpha10"]
                 ;; [metosin/reitit "0.5.15"]

                 ;; asynchronous programming
                 [manifold "0.1.9-alpha4"]
                 [org.clojure/core.async "1.3.618"]

                 ;; cryptography
                 [caesium "0.14.0"]
                 [buddy "2.0.0"]

                 ;; logging
                 [prone "2021-04-23"]
                 [com.taoensso/timbre "5.1.2"]

                 ;; debugging
                 [com.billpiel/sayid "0.1.0"]

                 ;; data
                 [cheshire "5.10.1"]
                 [selmer "1.12.44"]
                 [com.taoensso/nippy "3.1.1"]
                 [byte-streams "0.2.4"]
                 [primitive-math "0.1.6"]

                 ;; database
                 [org.xerial/sqlite-jdbc "3.36.0.1"]
                 [com.layerware/hugsql "0.5.1"]]
  :plugins [[cider/cider-nrepl "0.27.2"]
            [com.billpiel/sayid "0.1.0"]
            [jonase/eastwood "0.4.0"]
            [lein-ancient "0.7.0"]]
  :profiles {:uberjar {:aot :all}
             :test    {:plugins [[lein-ancient "0.7.0"]
                                 [lein-cljfmt "0.7.0"]
                                 [lein-kibit "0.1.8"]
                                 [jonase/eastwood "0.4.0"]]}}
  :main ^:skip-aot cldj.core
  :target-path "target/%s")
