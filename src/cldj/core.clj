(ns cldj.core
  (:require [clojure.edn :as edn]
            [aleph.http :as http]
            [byte-streams :as bs]
            [cheshire.core :as j]
            [manifold.stream :as s]
            [manifold.deferred :as d]
            [clojure.core.async :refer [chan close!]]
            [discljord.messaging :as discord-rest]
            [discljord.connections :as discord-ws]
            [discljord.formatting :refer [mention-user]]
            [discljord.events :refer [message-pump!]]
            [clojure.java.shell :refer [sh]])
  (:gen-class))

(def state (atom nil))

(def bot-id (atom nil))

(def config (edn/read-string (slurp (str (System/getProperty "user.home") "/.config.edn"))))

(def be-quiet (atom false))

(def sl (atom nil))

(def consumer (atom nil))

(defonce current-song (atom nil))

(def channel-ids ["591297729909489714"
                  "781970090035642368"])

(defn now-playing
  []
  (let [url        "https://sbf.technoanimal.net/status-json.xsl"
        collection (-> @(http/get url)
                       :body
                       bs/to-string
                       (j/parse-string true)
                       :icestats
                       :source)]
    (loop [collection collection]
      (if (empty? collection)
        nil
        (let [stream (first collection)
              server (:server_name stream)]
          (if (= server "Super Best Friends")
            (:title stream)
            (recur (rest collection))))))))

(defn listener-count
  []
  (let [url        "https://sbf.technoanimal.net/status-json.xsl"
        collection (-> @(http/get url)
                       :body
                       bs/to-string
                       (j/parse-string true)
                       :icestats
                       :source)]
    (loop [collection collection]
      (if (empty? collection)
        nil
        (let [stream (first collection)
              server (:server_name stream)]
          (if (= server "Super Best Friends")
            (:listeners stream)
            (recur (rest collection))))))))

(defn new-message
  [message]
  (println "Quietness:" @be-quiet)
  (println "Sending message:" message)
  (when (not @be-quiet)
    (doseq [channel-id channel-ids]
      (discord-rest/create-message! (:rest @state) channel-id :content message))))

(defn compare-and-set-song
  []
  (println "in compare-and-set-song")
  (let [song-atom @current-song
        np        (now-playing)
        listeners (listener-count)]
    (if (nil? np)
      true
      (when (and (not (= song-atom np))
                 (not (= 0 listeners)))
        (reset! current-song np)
        (new-message (str "SBF Radio [listeners: " listeners "]: " np "\nListen: https://sbf.technoanimal.net/sbf | DJ: https://sbf.technoanimal.net/dj/"))))))

(defn sleep-loop
  []
  (while true
    (do
      (println "In thread loop.")
      (Thread/sleep 1000)
      (compare-and-set-song))))

(defn start-bot! [token & intents]
  (let [event-channel      (chan 100)
        gateway-connection (discord-ws/connect-bot! token event-channel :intents (set intents))
        rest-connection    (discord-rest/start-connection! token)]
    {:events  event-channel
     :gateway gateway-connection
     :rest    rest-connection}))

(defn stop-bot! [{:keys [rest gateway events] :as _state}]
  (discord-rest/stop-connection! rest)
  (discord-ws/disconnect-bot! gateway)
  (close! events))

(defn mpc-call
  [call]
  (let [{retval :exit
         output :out} (sh "mpc" "-p" "6602" call)]
    (if (not (= 0 retval))
      (throw (Exception. "Non-zero return value from mpc."))
      (clojure.string/trim-newline output))))

(defn skip
  []
  (let [_ (mpc-call "skip")]
    (new-message "Skipping.")))

(defn clear
  []
  (let [_ (mpc-call "clear")
        _ (mpc-call "stop")]
    (new-message "Playback stopped, queue cleared.")))

(defn search
  [query]
  (let [query-results (:out (sh "mpc" "-p" "6602" "search" "any" query))
        query-results (clojure.string/join "\n" (take 10 (clojure.string/split query-results #"\n")))]
    (println query-results)
    (new-message (str query-results))))

(defn dispatch
  [message]
  (println "in dispatch")
  (println message)
  (cond (= message "radio on")
        (do (println "on")
            (reset! be-quiet false))

        (= message "radio off")
        (do (println "off")
            (reset! be-quiet true))))

(defn callback
  [message]
  (let [message-type     (first message)
        message-contents (second message)]
    (when (= message-type :message-create)
      (dispatch (:content message-contents)))))

(defn -main [& args]
  (reset! state (start-bot! (:token config) :guild-messages))
  (reset! bot-id (:id @(discord-rest/get-current-user! (:rest @state))))
  (reset! sl (d/future (sleep-loop)))
  (reset! consumer (s/consume #(callback %) (:events @state)))

  (while true
    (Thread/sleep 1000)))
